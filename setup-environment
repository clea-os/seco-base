#!/bin/bash

CWD=`pwd`
PROGNAME="setup-environment"
PACKAGE_CLASSES=${PACKAGE_CLASSES:-package_rpm}

usage() {
   echo "Usage: DISTRO=\$DISTRO MACHINE=\$MACHINE . ./$PROGNAME \$BUILD_DIR"
}

clean_up() {
   unset EULA LIST_MACHINES VALID_MACHINE
   unset CWD TEMPLATES SHORTOPTS LONGOPTS ARGS PROGNAME
   unset generated_config updated
   unset MACHINE SDKMACHINE DISTRO OEROOT
}

###############################################################################
#                                                                             #
#                              ARGUMENTS MANAGEMENT                           #
#                                                                             #
###############################################################################
# get command line options
SHORTOPTS="h"
LONGOPTS="help"

ARGS=$(getopt --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $PROGNAME -- "$@" )
# Print the usage menu if invalid options are specified
if [ $? != 0 -o $# -lt 1 ]; then
   usage && clean_up
   return 1
fi

eval set -- "$ARGS"
while true;
do
    case $1 in
        -h|--help)
           usage
           clean_up
           return 0
           ;;
        --)
           shift
           break
           ;;
    esac
done
###############################################################################


###############################################################################
#                                                                             #
#                              ENVIRONMENT CHECHING                           #
#                                                                             #
###############################################################################
if [ "$(whoami)" = "root" ]; then
    echo "ERROR: do not use the BSP as root. Exiting..."
fi

if [ ! -e $1/conf/local.conf.sample ]; then
    build_dir_setup_enabled="true"
else
    build_dir_setup_enabled="false"
fi

if [ "$build_dir_setup_enabled" = "true" ] && [ -z "$MACHINE" ]; then
    usage
    echo -e "ERROR: You must set MACHINE when creating a new build directory."
    clean_up
    return 1
fi

if [ -z "$SDKMACHINE" ]; then
    SDKMACHINE='i686'
fi

if [ "$build_dir_setup_enabled" = "true" ] && [ -z "$DISTRO" ]; then
    usage
    echo -e "ERROR: You must set DISTRO when creating a new build directory."
    clean_up
    return 1
fi
###############################################################################


OEROOT=$PWD/layers/poky
if [ -e $PWD/layers/oe-core ]; then
    OEROOT=$PWD/layers/oe-core
fi

. $OEROOT/oe-init-build-env $CWD/$1 > /dev/null

# if conf/local.conf not generated, no need to go further
if [ ! -e conf/local.conf ]; then
    clean_up && return 1
fi

# Clean up PATH, because if it includes tokens to current directories somehow,
# wrong binaries can be used instead of the expected ones during task execution
export PATH="`echo $PATH | sed 's/\(:.\|:\)*:/:/g;s/^.\?://;s/:.\?$//'`"
export PATH="$PATH:${CWD}"

generated_config=
if [ "$build_dir_setup_enabled" = "true" ]; then

    TEMPLATES=$CWD/layers/base/config/
###############################################################################
#                                                                             #
#                              GENERATE local.conf                            #
#                                                                             #
###############################################################################
    # Backup default local.conf
    mv conf/local.conf conf/local.conf.sample
    {
        # Generate the local.conf based on the Yocto defaults
        grep -v '^#\|^$' conf/local.conf.sample
    } > conf/local.conf

    # Change settings according environment
    sed -e "s,MACHINE ??=.*,MACHINE ??= '$MACHINE',g" \
        -e "s,SDKMACHINE ??=.*,SDKMACHINE ??= '$SDKMACHINE',g" \
        -e "s,DISTRO ?=.*,DISTRO ?= '$DISTRO',g" \
        -e "s,PACKAGE_CLASSES ?=.*,PACKAGE_CLASSES ?= '$PACKAGE_CLASSES',g" \
        -i conf/local.conf

    # Set sstate directory according to the environment
    if [[ ! -z $SSTATE_DIR ]]; then
        echo "SSTATE_DIR ?= \"${SSTATE_DIR}\"" >> conf/local.conf
    fi

    # Change download directory according environment
    {
        if [[ ! -z $DOWNLOAD_DIR ]]; then
            echo "DL_DIR ?= \"${DOWNLOAD_DIR}\""
        else
            echo "DL_DIR ?= \"\${BSPDIR}/downloads/\""
        fi
    } >> conf/local.conf

    # Set nice level according to the environment
    if [[ ! -z $NICE_LEVEL ]]; then
        echo "BB_NICE_LEVEL ?= \"${NICE_LEVEL}\"" >> conf/local.conf
    fi

    # Set the cloud configuration

    if [[ ! -z $ASTARTE_URL ]]; then
        echo "ASTARTE_URL ?= \"${ASTARTE_URL}\"" >> conf/local.conf
    fi

    if [[ ! -z $ASTARTE_REALM ]]; then
        echo "ASTARTE_REALM ?= \"${ASTARTE_REALM}\"" >> conf/local.conf
    fi

    if [[ ! -z $ASTARTE_PAIRING_TOKEN ]]; then
        echo "ASTARTE_PAIRING_TOKEN ?= \"${ASTARTE_PAIRING_TOKEN}\"" >> conf/local.conf
    fi

    # Set custom RAUC key and certificate

    if [[ ! -z $SIGNING_KEY_PATH ]]; then
        echo "RAUC_KEY_FILE = \"${SIGNING_KEY_PATH}\"" >> conf/local.conf
    fi

    if [[ ! -z $SIGNING_CERT_PATH ]]; then
        echo "RAUC_CERT_FILE = \"${SIGNING_CERT_PATH}\"" >> conf/local.conf

        CUSTOM_KEYRING=$(basename "${SIGNING_CERT_PATH}")
        CUSTOM_KEYRING_PATH=$(dirname "${SIGNING_CERT_PATH}")
        echo "CUSTOM_KEYRING ?= \"${CUSTOM_KEYRING}\"" >> conf/local.conf
        echo "CUSTOM_KEYRING_PATH ?= \"${CUSTOM_KEYRING_PATH}\"" >> conf/local.conf
    fi

    if [[ ! -z $DEVICETREE_OVERLAY_CARRIER ]]; then
        echo "DEFAULT_DEVICETREE_OVERLAY_CARRIER = \"${DEVICETREE_OVERLAY_CARRIER}\"" >> conf/local.conf
    fi

    # Setup packages mirror
    DISTRO_CODENAME=$(grep DISTRO_CODENAME ../layers/poky/meta-poky/conf/distro/poky.conf | cut -d'"' -f2)
    if [ -n "$DISTRO_CODENAME" ]; then
        SECO_PACKAGES_MIRROR="https://secostorage.blob.core.windows.net/secosoftware-public/clea-os/mirror/$DISTRO_CODENAME"

        {
            # If a recipe source isn't available upstream anymore, we need
            # to fall back to our mirror server. The MIRROR variable is
            # evaluated last, after the local copy, the PREMIRRORS, and
            # the upstream version (also see the Yocto Glossary for more
            # details).
            echo
            echo "PREMIRRORS:prepend = \" \\"
            echo "    ftp://.*/.* $SECO_PACKAGES_MIRROR/ \\"
            echo "    git://.*/.* $SECO_PACKAGES_MIRROR/ \\ "
            echo "    gitsm://.*/.* $SECO_PACKAGES_MIRROR/ \\ "
            echo "    http://.*/.* $SECO_PACKAGES_MIRROR/ \\ "
            echo "    https://.*/.* $SECO_PACKAGES_MIRROR/ \\ "
            echo "\""
            echo
            echo "MIRRORS:prepend = \" \\"
            echo "    ftp://.*/.* $SECO_PACKAGES_MIRROR/archive/ \\"
            echo "    git://.*/.* $SECO_PACKAGES_MIRROR/archive/ \\ "
            echo "    gitsm://.*/.* $SECO_PACKAGES_MIRROR/archive/ \\ "
            echo "    http://.*/.* $SECO_PACKAGES_MIRROR/archive/ \\ "
            echo "    https://.*/.* $SECO_PACKAGES_MIRROR/archive/ \\ "
            echo "\""

        } >> conf/local.conf
    fi

    # Within the SECO, there are optimized manual build servers
    # equipped with preconfigured sstate cache and package directories.
    # Set the appropriate variables to utilize these resources efficiently.
    SSTATE_MIRROR_DIRS="/var/cache/cleaos/sstate_cache_mirror"
    DL_DIR_GLOBAL="/usr/src/cleaos_packages"

    if [ -d "$SSTATE_MIRROR_DIR" ] && [ -r "$SSTATE_MIRROR_DIR" ];then
       {
            # Generate list for chache mirrors
            echo ""
            echo "SSTATE_MIRRORS = \"\\"

            # Find subdirectories containing 'sstate-*'
            found_subdirs=
            for subdir in "$SSTATE_MIRROR_DIRS"/*/; do
                subdir_name=$(basename "$subdir")
                if [[ -d "$subdir" && "$subdir_name" == sstate-* ]]; then
                    echo "    file://.* file://$SSTATE_MIRROR_DIRS/$subdir_name/PATH \\"
                    found_subdirs=1
                fi
            done

            # If no sstate-* subdirectories found, use the parent directory
            if [ -z "$found_subdirs" ]; then
                echo "    file://.* file://$SSTATE_MIRROR_DIRS/PATH \\"
            fi

            echo "\""
            echo ""

            # USe CI's download dir as local mirror
            if [ -d "$DL_DIR_GLOBAL" ] && [ -w "$DL_DIR_GLOBAL" ];then
                echo 'INHERIT += "own-mirrors"'
                echo "SOURCE_MIRROR_URL = \"file://$DL_DIR_GLOBAL\""
                echo "DL_DIR ?= \"/home/$USER/cleaos_downloads\""
            fi
       } >> conf/local.conf
    fi

    # Include the SRCREV.conf file content if the file exists.
    # The SRCREV.conf file is searched in three different places, in order of priority:
    # - the custom layer, if any
    # - the layer where the used MACHINE configuration file is stored
    # - the ARCHITECTURE layer to which the MACHINE belongs
    SRCREV_SEARCH_PATHS=

    if [[ ! -z $CUSTOM_LAYER ]]; then
        SRCREV_SEARCH_PATHS+=" $CWD/layers/meta-seco/meta-$CUSTOM_LAYER/conf/SRCREV.conf "
    fi

    for layer in $CWD/layers/meta-seco/meta-seco-* ; do
        if [[ -e ${layer}/conf/machine ]]; then
            find=$(find ${layer}/conf/machine -name ${MACHINE}.conf | wc -l)
            if [[ $find -eq 1 ]]; then
                SRCREV_SEARCH_PATHS+=" $layer/conf/SRCREV.conf "
            fi
        fi
    done

    SRCREV_SEARCH_PATHS+=" $CWD/layers/meta-seco/meta-seco-$ARCHITECTURE/conf/SRCREV.conf "

    for SRCREV_FILEPATH in $SRCREV_SEARCH_PATHS; do
        if [[ -f $SRCREV_FILEPATH ]]; then
            cp "$SRCREV_FILEPATH" conf/
            sed -i '1s/^/include SRCREV.conf\n\n/' conf/local.conf
            break
        fi
    done

###############################################################################

###############################################################################
#                                                                             #
#                            GENERATE bblayers.conf                           #
#                                                                             #
###############################################################################
    {
        cat $TEMPLATES/bblayers.conf | head -$((`cat $TEMPLATES/bblayers.conf | wc -l`)) > conf/bblayers.conf
        echo ""
        cat $CWD/layers/meta-seco/meta-seco-$ENVIRONMENT/conf/bblayers.conf
        echo ""
        cat $CWD/layers/meta-seco/meta-seco-$ARCHITECTURE/conf/bblayers.conf
        echo "" 
        if [[ ! -z ${CUSTOM_LAYER} ]]; then
            if [[ ! -e ${CUSTOM_BBLAYER} ]]; then
                cat $CWD/$CUSTOM_BBLAYER
                echo "" 
            fi
        fi   
    } >> conf/bblayers.conf
###############################################################################

###############################################################################
#                                                                             #
#                             HANDLE EULA SETTING                             #
#                                                                             #
###############################################################################
# TEMPORARY: Automatically accepted FSL eula
   {
        if [[ "$ARCHITECTURE" == "imx" ]]; then
            echo
            echo "ACCEPT_FSL_EULA = \"1\""
        fi
    }  >> conf/local.conf
###############################################################################

    generated_config=1
fi

###############################################################################
#                                                                             #
#                               WELCOME MESSAGE                               #
#                                                                             #
###############################################################################
cat <<EOF

Welcome to SECO BSP

The Yocto Project has extensive documentation about OE including a
reference manual which can be found at:
    http://yoctoproject.org/documentation

For more information about OpenEmbedded see their website:
    http://www.openembedded.org/

EOF

if [ -n "$generated_config" ]; then
    cat <<EOF
Your build environment has been configured with:

    MACHINE=$MACHINE
    SDKMACHINE=$SDKMACHINE
    DISTRO=$DISTRO
    EULA=$EULA
EOF
else
    echo "Your configuration files at $1 have not been touched."
fi

clean_up
