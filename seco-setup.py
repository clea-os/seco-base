#!/usr/bin/env python3

from multiprocessing import current_process
import subprocess
import os
import sys
import stat
import argparse
from configparser import ConfigParser
import traceback
import shutil
import logging
from yaml import parse

distro = None
machine = None
environment = None
architecture = None
build_dir = None
download_dir = None
sstate_dir = None
nice_level = None
astarte_url = None
astarte_realm = None
astarte_pairing_token = None
signing_key_path = None
signing_cert_path = None
devicetree_overlay_carrier = None

# Operation
new_project = 0
use_oldconfig = 0

logging.basicConfig(filename='seco-setup.log', level=logging.INFO)

####################################################
#                 CMD LINE ARGS                    #
####################################################
parser = argparse.ArgumentParser(description="This script handle the configuration of your YOCTO project.",add_help=False)
parser.add_argument("-h", "--help", action='store_true', help="Use this option to print this message.")
parser.add_argument("-d", "--defconfig", type=str, help="Use existing configuration to create a new Yocto project.")
parser.add_argument("-m", "--menuconfig", action='store_true', help="Open menuconfig user interface to configure current configuration")
parser.add_argument("-l", "--use_last", action='store_true', help="Start the configuration tool create a new Yocto project.")
parser.add_argument("-c", "--get_configuration", action='store_true', help="Return the setting stored in .config file.")
parser.add_argument("-p", "--get_config_path", action='store_true', help="Return the path of the current configuration file.")
parser.add_argument('-a', "--add_custom_layer", type=str, help="Add an extra layer to default setting.")


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def getDefconfigCustomLayer(custom_layer):
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/"
    meta_layer = "meta-" + custom_layer
    abs_path = os.path.join(abs_path, "../meta-seco/" + meta_layer + "/conf/configs")
    return abs_path


def checkMenuconfigToolsExists():
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/"
    mconf = os.path.join(abs_path, "mconf")
    conf = os.path.join(abs_path, "conf")
    if not os.path.exists(mconf):
        return False
    if not os.path.exists(conf):
        return False
    return True


def compileMenuconfigTools():
    logging.debug('Building the menuconfig tool')
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/"
    tool = os.path.join(abs_path, "menuconfig")
    if not (os.path.isdir(tool) and len(os.listdir(tool)) != 0):
        logging.error('The menuconfig source folder is empty.'
                      'Maybe the submodule hasn\'t been initialized?')
        return False
    build_dir = os.path.join(abs_path, "build")
    mconf = os.path.join(build_dir, "mconf")
    conf = os.path.join(build_dir, "conf")
    logging.debug('Build directory for the menuconfig tool: %s', build_dir)
    if not os.path.isdir(build_dir):
        os.mkdir(build_dir)
    if not os.path.isdir(build_dir):
        return False
    subprocess.call("cmake ../menuconfig && make",stderr=subprocess.PIPE,stdout=subprocess.PIPE,shell=True,cwd=build_dir)
    shutil.copyfile(mconf, "mconf")
    os.chmod("mconf", stat.S_IXUSR | stat.S_IXGRP)
    shutil.copyfile(conf, "conf")
    os.chmod("conf", stat.S_IXUSR | stat.S_IXGRP)
    shutil.rmtree(build_dir)
    return True


def runMenuConfig(custom_layer):
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/"
    mconf = os.path.join(abs_path, "mconf")
    conf = abs_path + "config/Yconfig/Yconfig"
    if ( custom_layer != None ):
       mconf = "CUSTOM=" + custom_layer + " " + mconf
    # TODO: Calling xterm at this point only works if we are on a local Linux desktop.
    # On headless systems or when working remotely on a build server (e.g. via SSH),
    # the command fails. The reason for using xterm here is that the seco-setup.sh calls
    # the Python script with command substitution, because it needs the name of the new
    # config as a return value. Nevertheless, a different approach might be better to
    # make the setup scripts more independent from the build system setup.
    subprocess.call("xterm -geometry 127x40 -fa 'Monospace' -fs 9 -e \"" + mconf + " " + conf + "\"",shell=True,cwd=abs_path)
      
      
def runDefconfig(defconfig, custom_layer): 
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/"
    tool = os.path.join(abs_path, "conf")
    if ( custom_layer != None ):
        tool = "CUSTOM=" + custom_layer + " " + tool
    conf = abs_path + "config/Yconfig/Yconfig"
    cmd = tool +  " --defconfig=" + defconfig + " " + conf
    subprocess.call(cmd,stderr=subprocess.PIPE,stdout=subprocess.PIPE,shell=True,cwd=build_dir)
    
    
def extractLabelFromLine(line, tag):
    tag = "CONFIG_" + tag.upper() + "_"
    if line.startswith(tag):
        if line.endswith("=y"):
            label = line[len(tag):len(line)-2]
            label = label.replace("_", "-")
            label = label.lower()
            return label

    return None


def extractValueFromLine(line, tag):
    tag = "CONFIG_" + tag.upper() + "="
    if line.startswith(tag):
        value = line[len(tag):len(line)]
        if value.startswith('"') and value.endswith('"'):
            value = value[1:len(value)-1].strip()
        return value

    return None


def parseConfigLine(line):
    text = extractLabelFromLine(line, "distro")
    if text != None:
        global distro
        distro = text
        return
    text = extractLabelFromLine(line, "machine")
    if text != None:
        global machine
        machine = text
        return
    text = extractLabelFromLine(line, "environment")
    if text != None:
        global environment
        if text.startswith("seco-"):
            text = text[5:len(text)]
        environment = text
        return
    text = extractLabelFromLine(line, "architecture")
    if text != None:
        global architecture
        architecture = text
        return
    text = extractValueFromLine(line, "build_dir")
    if text != None:
        global build_dir
        build_dir = text
        return
    text = extractValueFromLine(line, "dl_dir")
    if text != None:
        global download_dir
        download_dir = text
        return
    text = extractValueFromLine(line, "sstate_dir")
    if text != None:
        global sstate_dir
        sstate_dir = text
        return
    text = extractValueFromLine(line, "nice_level")
    if text != None:
        global nice_level
        nice_level = text
        return
    text = extractValueFromLine(line, "astarte_url")
    if text != None:
        global astarte_url
        astarte_url = text
        return
    text = extractValueFromLine(line, "astarte_realm")
    if text != None:
        global astarte_realm
        astarte_realm = text
        return
    text = extractValueFromLine(line, "astarte_pairing_token")
    if text != None:
        global astarte_pairing_token
        astarte_pairing_token = text
        return
    text = extractValueFromLine(line, "signing_key_path")
    if text != None:
        global signing_key_path
        signing_key_path = text
        return
    text = extractValueFromLine(line, "signing_cert_path")
    if text != None:
        global signing_cert_path
        signing_cert_path = text
        return
    text = extractValueFromLine(line, "devicetree_overlay_carrier")
    if text != None:
        global devicetree_overlay_carrier
        devicetree_overlay_carrier = text
        return

def parseConfig():
    try:
        with open(".config", "r") as config:
            lines = config.readlines()
    except OSError:
        eprint("ERROR: Could not open config file")
        return False

    for line in lines:
        line = line.strip()
        if len(line) > 0 and line[0] != "#":
            parseConfigLine(line)

    return True


def backupConfig():
    abs_path = os.path.dirname( os.path.realpath(__file__) )
    config_file = os.path.join(abs_path, ".config")
    current_file = os.path.join(abs_path, ".current_config")
    if os.path.exists(config_file):
        os.rename(config_file, current_file)


def useBackupConfig():
    abs_path = os.path.dirname( os.path.realpath(__file__) )
    config_file = os.path.join(abs_path, ".config")
    current_file = os.path.join(abs_path, ".current_config")
    if os.path.exists(current_file):
        shutil.copyfile(current_file, config_file)


def defconfigExists(defconfig):
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/configs"
    defconifg_file = os.path.join(abs_path, defconfig)
    if not os.path.exists(defconifg_file):
        return False
    return True


def getConfigPath():
    abs_path = os.path.dirname( os.path.realpath(__file__) )
    config_file = os.path.join(abs_path, ".config")
    current_file = os.path.join(abs_path, ".current_config")
    if os.path.exists(config_file):
        return config_file
    elif os.path.exists(current_file):
        return current_file
    else:
        return ""
    
    
def runDefconfigProcedure(defconfig, custom_layer):
    if not checkMenuconfigToolsExists():
        logging.info('Need to build menuconfig tool')
        if not compileMenuconfigTools():
            logging.error('Failed to build menuconfig tool')
            return False
    abs_path = os.path.dirname( os.path.realpath(__file__) ) + "/configs"
    defconifg_file = os.path.join(abs_path, defconfig)
    if not os.path.exists(defconifg_file):
        if ( custom_layer != None ):
            abs_path = getDefconfigCustomLayer(custom_layer)
            defconifg_file = os.path.join(abs_path, defconfig)
            if not os.path.exists(defconifg_file):
                return False
        else:
            return False
    runDefconfig(defconifg_file, custom_layer)
    return True
    

def runMenuconfigProcedure(use_old, custom_layer):
    if not checkMenuconfigToolsExists():
        logging.info('Need to build menuconfig tool')
        if not compileMenuconfigTools():
            logging.error('Failed to build menuconfig tool')
            return False
    if use_old:
        useBackupConfig()
    runMenuConfig(custom_layer)
    return True


def runGetConfigurationProcedure():
    set_config_string = True
    parseConfig()
    if parseConfig() == True:
        backupConfig()
        if distro == None:
            eprint("ERROR: no distro found")
            set_config_string = False
        if machine == None:
            eprint("ERROR: no machine found")
            return
        if environment == None:
            eprint("ERROR: no environment found")
            set_config_string = False
        if architecture == None:
            eprint("ERROR: no architecture found")
            set_config_string = False
        if build_dir == None or len(build_dir) == 0:
            eprint("ERROR: no build dir found")
            set_config_string = False
        if download_dir == None:
            eprint("ERROR: no download directory found")
            set_config_string = False
        if sstate_dir == None:
            eprint("ERROR: no sstate directory found")
            set_config_string = False
        if nice_level == None:
            eprint("WARN: no nice level defined")
        if astarte_url == None:
            eprint("WARN: no cloud URL set")
        if astarte_realm == None:
            eprint("WARN: no cloud realm set")
        if astarte_pairing_token == None:
            eprint("WARN: no cloud pairing token set")
        if signing_key_path == None:
            eprint("WARN: no custom signing key set")
        if signing_cert_path == None:
            eprint("WARN: no custom signing cert set")
    else:
        print("ERROR: unable to parse config file")
        return

    conf_string = ""
    if set_config_string == True:
        conf_string += "distro=" + distro
        conf_string += ":" + "machine=" + machine
        conf_string += ":" + "architecture=" + architecture
        conf_string += ":" + "environment=" + environment
        conf_string += ":" + "build_dir=" + build_dir
        conf_string += ":" + "download_dir=" + download_dir
        conf_string += ":" + "sstate_dir=" + sstate_dir
        conf_string += ":" + "nice_level=" + nice_level
        if astarte_url != None:
            conf_string += ":" + "astarte_url=" + astarte_url
        if astarte_realm != None:
            conf_string += ":" + "astarte_realm=" + astarte_realm
        if astarte_pairing_token != None:
            conf_string += ":" + "astarte_pairing_token=" + astarte_pairing_token
        if signing_key_path  != None:
            conf_string += ":" + "signing_key_path=" + signing_key_path
        if signing_cert_path  != None:
            conf_string += ":" + "signing_cert_path=" + signing_cert_path
        if astarte_url != None:
            conf_string += ":" + "astarte_url=" + astarte_url
        if astarte_realm != None:
            conf_string += ":" + "astarte_realm=" + astarte_realm
        if astarte_pairing_token != None:
            conf_string += ":" + "astarte_pairing_token=" + astarte_pairing_token
        if devicetree_overlay_carrier != None:
            conf_string += ":" + "devicetree_overlay_carrier=" + devicetree_overlay_carrier

    print ( conf_string )
    return set_config_string


def main():
    custom_layer = None
    
    try:
        ### cmd line arguments parser
        args = parser.parse_args()
        
        #####################################################################
        #                        -h  /  --help                              #
        #####################################################################
        if args.help:
            parser.print_help()
            return True
        
        ####################################################################
        #                      -a  /  --add_custom_layer                   #
        ####################################################################
        if args.add_custom_layer != None:
            custom_layer = args.add_custom_layer
            
        ####################################################################
        #                      -d  /  --defconfig                          #
        ####################################################################
        if args.defconfig:
            if runDefconfigProcedure(args.defconfig, custom_layer):
                print( args.defconfig )
                return True
            else:
                return False
            
        ####################################################################
        #                      -m  /  --menuconfig                         #
        ####################################################################
        if args.menuconfig:
                # if args.use_last:
                #     useBackupConfig()
            return runMenuconfigProcedure(False, custom_layer)
            #return True
            
        
        ####################################################################
        #                    -c  /  --get_configuration                    #
        ####################################################################
        if args.get_configuration:
            return runGetConfigurationProcedure()          
        
        ####################################################################
        #                      -p  /  --get_config_path                    #
        ####################################################################
        if args.get_config_path:
            path = getConfigPath()
            print(path)
            return True
        
        return 0
    except:

        print(traceback.format_exc())
        sys.exit(1)
    
    
if __name__ == '__main__':
    ret = main()
    sys.exit(ret)
