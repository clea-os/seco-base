#!/bin/bash

#EXEC_FILE=`basename $0`
EXEC_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/
SCRIPT_FULL=`readlink -f ${BASH_SOURCE[0]}`
SCRIPT_PATH=`dirname $SCRIPT_FULL`
WIZARD_PATH=./seco-setup.py
OLD_CONFIG=./.current_config
DEFCONFIG_FOLDER=${SCRIPT_PATH}/configs/

#LIB_PATH=${SCRIPT_PATH}/lib/

CWD=`pwd`
PROGNAME=$(basename ${BASH_SOURCE[0]})

LAYER_PATH=./layers
CUSTOM_FILE=custom_layer
CUSTOM_LAYER=
CUSTOM_DEFCONFIGS=
CUSTOM_BBLAYER=
arg_custom=

#source ${LIB_PATH}stdio.sh


DISTRO=
MACHINE=
ENVIRONMENT=
ARCHITECTURE=
BUILD_DIR=
DOWNLOAD_DIR=
SSTATE_DIR=
NTHREAD=
NICE_LEVEL=
ASTARTE_URL=
ASTARTE_REALM=
ASTARTE_PAIRING_TOKEN=
SIGNING_KEY_PATH=
SIGNING_CERT_PATH=
DEVICETREE_OVERLAY_CARRIER=

usage () {
    echo -e "
Usage: source $PROGNAME <commnad> [options]

Command list:
    -h, --help                  Prints this message and quit.
    -l, --list_defconfig        Prints list of all default configuration that can be used and quit.
    -d, --defconfig [defconf]   New Yocto project created with setting stored into defconfig
                                file passed as argument (among those available).
                                For defconf string refer to list available with -l command.
    -m, --menuconfig [ptions]   Open menuconfig user interface. Uses the current configuration as
                                start point. If a configuration is not present, the default
                                setting will be used.
    -c, --create [options]      New Yocto project created with current configuration setting
    -r, --reconfigure           Insede a Yocto project, allows to change configuration
    
    

[menuconfig] specific options:
    -o, --use_last              Allows to start from the current used configuration.

[defconfig] specific options:
    -b, --build                 Allows to start from the current used configuration.
"
}

is_sourced() {
    if [ -n "$ZSH_VERSION" ]; then 
        case $ZSH_EVAL_CONTEXT in *:file:*) return 0;; esac
    else  # Add additional POSIX-compatible shell names here, if needed.
        case ${0##*/} in dash|-dash|bash|-bash|ksh|-ksh|sh|-sh) return 0;; esac
    fi
    return 1  # NOT sourced.
}


clean_up() {
   unset EULA LIST_MACHINES VALID_MACHINE
   unset CWD TEMPLATES SHORTOPTS LONGOPTS ARGS PROGNAME
   unset generated_config updated
   unset MACHINE SDKMACHINE DISTRO OEROOT
}


runGetCustomLayer() {
    [[ ! -e ${CUSTOM_FILE} ]] && return
    CUSTOM_LAYER=$(cat ${CUSTOM_FILE})
    if [[ ! -d ${LAYER_PATH}/meta-seco/meta-${CUSTOM_LAYER} ]]; then
cat << EOF

***
    WARNING: "$CUSTOM_LAYER" is an invalid layer!!! Ignore it.
***

EOF
        return
    fi


    if [[ -z $CUSTOM_LAYER ]]; then
        arg_custom=""
    else
cat << EOF

***
    INFO: Found custom layer "${CUSTOM_LAYER}"!!! Use it.
***

EOF
        arg_custom="--add_custom_layer=${CUSTOM_LAYER}"
        CUSTOM_DEFCONFIGS=${LAYER_PATH}/meta-seco/meta-${CUSTOM_LAYER}/conf/configs/
        CUSTOM_BBLAYER=${LAYER_PATH}/meta-seco/meta-${CUSTOM_LAYER}/conf/bblayers.conf
    fi
}


is_build_folder() {
    if [[ -z $BBPATH ]]; then
        return 0
    fi
    if [[ $BBPATH != ${CWD} ]]; then
        return 0
    fi
    return 1
}


print_table () {
    local defconfigs=$(ls $DEFCONFIG_FOLDER)
    if [[ ! -z ${CUSTOM_LAYER} ]] && [[ -d ${CUSTOM_DEFCONFIGS} ]]; then
        local defconfigs_custom=$(ls ${CUSTOM_DEFCONFIGS})
        defconfigs+=(${defconfigs_custom[@]})
    fi
    local descriptions=
    local max1=0
    local max2=0
    local description=
    for conf in ${defconfigs[@]}; do
        if [[ -e $DEFCONFIG_FOLDER/$conf ]]; then
            description="$(cat $DEFCONFIG_FOLDER/$conf | grep -i "@Description" | awk -F ':' '{print $2}' | xargs)"
        else
            description="$(cat ${CUSTOM_DEFCONFIGS}/$conf | grep -i "@Description" | awk -F ':' '{print $2}' | xargs)"
        fi
        local descriptions+=("$description")
        local n=$(expr length "$conf")
        if [[ $max1 -lt $n ]]; then
            local max1=$n
        fi
        local n=$(expr length "$description")
        if [[ $max2 -lt $n ]]; then
            local max2=$n
        fi
    done
    max1=$((max1 + 5))
    max2=$((max2 + $(expr length "Description")))
    echo
    echo
    local separator1=$(printf '%*s' "$max1" | tr ' ' "-")
    local separator2=$(printf '%*s' "$max2" | tr ' ' "-")
    paste <(printf '%s\n%s\n' Configuration $separator1 "${defconfigs[@]//_defconfig/}") \
          <(printf '%s\n%s\n' Description $separator2 "${descriptions[@]}") \
    | column -ts $'\t' 
    echo
    echo
}

is_sourced && sourced=1 || sourced=0
if [[ $sourced -ne 1 ]]; then
    echo 
    echo "ERROR: the script `basename $0` has not been sourced. You have to run it as a sourced script.";
    echo "EXIT."
    echo
    exit 1
fi
 
 
###############################################################################
#                                                                             #
#                           GET COMMAND LINE OPTIONS                          #
#                                                                             #
###############################################################################

# Avalaible operations
project_menuconfig=0        # Open menuconfig interface, starting current config or
                            # from default configuration or with previous used configuration
project_defconfig=0         # New configuration is created with setting stored into defconfig
                            # file passed as argument (among those available)
project_create=0            # Create a new Yocto project with the setting stored in .config file
project_reconfigure=0       # Setting re-configuration of the Yocto project you are in


# Available options
use_last=0      # used with "new" operation. Allows to use the current used configuration
                # If equals to 0 the user starts from default configuration.
                # if equals to 1 the user starts from setting stored the .config_old file
defconfig=      # defconfig file to use to create a new Yocto project

# $@ is all command line parameters passed to the script.
# -o is for short options like -h
# -l is for long options with double dash like --help
# the comma separates different long options
# -a is for long options with single dash like -help
options=$(getopt -l "help,list_defconfig,defconfig:,menuconfig,use_last,create,reconfigure" -o "hld:mocr" -a -- "$@")

# set --:
# If no arguments follow this option, then the positional parameters are unset. Otherwise, the positional parameters 
# are set to the arguments, even if some of them begin with a ‘-’.
eval set -- "$options"

runGetCustomLayer

while true; do
    case $1 in
# [help] command
        -h|--help) 
            usage
            return 0
            ;;
# [list_defconfig] command
        -l|--list_defconfig)
            print_table
            return 0
            ;;
# [defconfig] command
        -d|--defconfig)
            project_defconfig=1
            shift
            defconfig=$1
            ;;
# [menuconfig] command
        -m|--menuconfig)
            project_menuconfig=1
            ;;
        -o|--use_last)
            use_last=1
            ;;
# [create] command
        -c|--create)
            project_create=1
            ;;
# [reconfigure] command
        -r|--reconfigure)
            project_reconfigure=1
            ;;
        --)
            shift
            break
            ;;
    esac
    shift
done


###############################################################################


runMenuconfig() {
cat << EOF

***
    SECO provides a menuconfig interfaces to create new Yocto project.
    Please set the wanted configuration and save it.
    Through this menu you can select:
    - machine e distro 
    - Yocto's project parameters
    - HW specific configuration
    - Cloud connection setting
***

EOF
    cd $SCRIPT_PATH
    if [[ ${use_last} -eq 0 ]]; then        
        conf=$( $WIZARD_PATH ${arg_custom} --menuconfig  )
    else
        echo
        if [[ -f $OLD_CONFIG ]]; then
            echo "Start form previous configuration."
            conf=$( $WIZARD_PATH ${arg_custom} --menuconfig )
        else
            echo "WARNING: doesn't exist a previous configuration. Use default one."
            conf=$( $WIZARD_PATH ${arg_custom} --menuconfig --use_last )
        fi
        echo
    fi
    cd $EXEC_DIR 
}


runCreate() {
    cd $SCRIPT_PATH
    conf=$( $WIZARD_PATH ${arg_custom} --get_configuration )
    cd $EXEC_DIR
    error=$(echo $conf | head -n1 | awk -F':' '{print $1;}')
     if [[ "$error" == "ERROR" ]]; then
cat << EOF

***
    ${conf}

    Yocto project not created:
    Check if a valid configuration exists.
    For more info run
        source $PROGNAME --help
***

EOF
     else
        IFS=':' read -r -a arr <<< "$conf"
        for c in ${arr[@]}; do
            target=$( echo $c | awk -F '=' '{print $1}' )
            value=$( echo $c | awk -F '=' '{print $2}' )
            case ${target} in
                "distro") DISTRO=$value ;;
                "machine") MACHINE=$value ;;
                "environment") ENVIRONMENT=$value ;;
                "architecture") ARCHITECTURE=$value ;;
                "download_dir") DOWNLOAD_DIR=$value ;;
                "build_dir") BUILD_DIR=$value ;;
                "sstate_dir") SSTATE_DIR=$value ;;
                "nthread") NTHREAD=$value ;;
                "nice_level") NICE_LEVEL=$value ;;
                "astarte_url") ASTARTE_URL=$value ;;
                "astarte_realm") ASTARTE_REALM=$value ;;
                "astarte_pairing_token") ASTARTE_PAIRING_TOKEN=$value ;;
                "signing_key_path") SIGNING_KEY_PATH=$value ;;
                "signing_cert_path") SIGNING_CERT_PATH=$value ;;
                "devicetree_overlay_carrier") DEVICETREE_OVERLAY_CARRIER=$value ;;
            esac
        done
    
        # echo "@@@@@@@@@@@@@@@@@@@@@"
        # echo $DISTRO
        # echo $MACHINE
        # echo $ENVIRONMENT
        # echo $ARCHITECTURE
        # echo $BUILD_DIR
        # echo $DOWNLOAD_DIR
        # echo $SSTATE_DIR
        # echo $NTHREAD
        # echo $NICE_LEVEL
        # echo $ASTARTE_URL
        # echo $ASTARTE_REALM
        # echo $ASTARTE_PAIRING_TOKEN
        # echo $SIGNING_KEY_PATH
        # echo $SIGNING_CERT_PATH
        # echo $DEVICETREE_OVERLAY_CARRIER
        # echo "@@@@@@@@@@@@@@@@@@@@@"

        cd $SCRIPT_PATH
        conf_file=$( $WIZARD_PATH --get_config_path )
        cd $EXEC_DIR

        BUILDDIR=${CWD}/${BUILD_DIR}

        DISTRO=$DISTRO MACHINE=$MACHINE . ${SCRIPT_PATH}/setup-environment $BUILD_DIR

        if [[ -d ${BUILDDIR}/conf ]]; then
            ln -s $SCRIPT_FULL ${BUILDDIR}
            cp ${conf_file} ${BUILDDIR}/conf/.config
        fi

        # Point to the current directory since the last command changed the directory to $BUILD_DIR
        BUILD_DIR=.
    fi
}


###############################################################################
#                                                                             #
#                                 MENUCONFIG                                  #
#                                                                             #
###############################################################################
if [[ ${project_menuconfig} -eq 1 ]]; then
    is_build_folder
    ret=$?
    if [[ $ret -eq 1 ]]; then
cat << EOF

***
    ERROR: This command is not available when you are in a Yocto project
***

EOF
        return 1
    fi

    runMenuconfig
fi
###############################################################################


###############################################################################
#                                                                             #
#                                  DEFCONFIG                                  #
#                                                                             #
###############################################################################
if [[ ${project_defconfig} -eq 1 ]]; then
    full_path=
    defconfigs=$(ls $DEFCONFIG_FOLDER)
    find=0
    for conf in ${defconfigs}; do
        if [[ "${conf/_defconfig/}" == "${defconfig}" ]]; then
            find=1
            full_path=${DEFCONFIG_FOLDER}/${conf}
	        break
        fi
    done
    if [[ ${find} -eq 0 ]]; then
        if [[ ! -z ${CUSTOM_LAYER} ]] && [[ -d ${CUSTOM_DEFCONFIGS} ]]; then
            defconfigs=$(ls ${CUSTOM_DEFCONFIGS})
            for conf in ${defconfigs}; do
                if [[ "${conf/_defconfig/}" == "${defconfig}" ]]; then
                    find=1
                    full_path=${CUSTOM_DEFCONFIGS}/${conf}
                    break
                fi
            done
        fi
    fi

    if [[ ${find} -eq 1 ]]; then
        description="$(cat ${full_path} | grep -i "@Description" | awk -F ':' '{print $2}' | xargs)"
cat << EOF

***
    USE defconfig configuration file: ${defconfig}
    ${description}
***

EOF
    cd $SCRIPT_PATH
    conf=$($WIZARD_PATH ${arg_custom} --defconfig ${defconfig}_defconfig)
    cd $EXEC_DIR
    if [[ "${conf}" != "${defconfig}_defconfig" ]];then
cat << EOF

***
    ERROR: configuration ${defconfig} not found!!!
***

EOF
    fi
    else
cat << EOF

***
    ERROR: configuration ${defconfig} not found!!!
***

EOF
    fi
fi
###############################################################################


###############################################################################
#                                                                             #
#                                    CREATE                                   #
#                                                                             #
###############################################################################
if [[ ${project_create} -eq 1 ]]; then
   runCreate
fi
###############################################################################


###############################################################################
#                                                                             #
#                                 RECONFIGURE                                 #
#                                                                             #
###############################################################################
if [[ ${project_reconfigure} -eq 1 ]]; then
    echo RECONFIGURE
    echo $SCRIPT_FULL
    echo $EXEC_DIR
    if [[ -f ${EXEC_DIR}/conf/.config ]]; then
        cp ${EXEC_DIR}/conf/.config $SCRIPT_PATH
        runMenuconfig
        runCreate
    else
cat << EOF

***
    ERROR: local configuration file not present"
***

EOF
    fi
fi
###############################################################################



if [[ ${project_create} -eq 0 && ${project_menuconfig} -eq 0 && \
${project_defconfig} -eq 0 && ${project_reconfigure} -eq 0 ]]; then
    if [[ -d $1 ]]; then
        . ${SCRIPT_PATH}/setup-environment $1
    else
cat << EOF

***
    ERROR: $1 is not a Yocto project folder!!!
***

EOF
    fi
fi
