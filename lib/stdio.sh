#!/bin/bash

VERBOSE=0
DEBUG=0

function echo_d () {
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - ${FUNCNAME[1]}:${BASH_LINENO[$FUNCNAME[1]]} - D ]: "
	else
		verbose_str="* "
	fi
	if [ $DEBUG = 1 ]; then
		echo -e "\e[33m$verbose_str$1\e[39m"
	else
		return
	fi
}


function echo_w () {
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - ${FUNCNAME[1]}:${BASH_LINENO[$FUNCNAME[1]]} - W ]: "
	else
		verbose_str="* WARNING: "
	fi
	echo -e "\e[33m$verbose_str$1\e[39m"
}


function echo_e () {
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - ${FUNCNAME[1]}:${BASH_LINENO[$FUNCNAME[1]]} - E ]: "
	else
		verbose_str="* ERROR: "
	fi
	echo -e "\e[31m$verbose_str$1\e[39m"
}


function echo_ok () {
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - OK]: "
	else
		verbose_str="* "
	fi
	echo -e "\e[32m$verbose_str$1\e[39m"
}


function process_ok () {
	echo -e "\e[32m OK\e[39m"
}


function process_ko () {
	echo -e "\e[31m KO\e[39m"
}


function echo_i () {
	local verbose_str=""
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - I ]: "
	else
		verbose_str="* "
	fi
	echo -e "$verbose_str$1"
}


function echo_i_h () {
	local verbose_str=""
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - I ]: "
	else
		verbose_str="* "
	fi
	echo -e "\e[32m $verbose_str$1 \e[39m"
}


function echo_i_n () {
	local verbose_str=""
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - I ]: "
	else
		verbose_str="* "
	fi
	echo -e -n "$verbose_str$1"
}


function echo_i_h_n () {
	local verbose_str=""
	if [ $VERBOSE = 1 ]; then
		verbose_str="[`date +"%T"` - I ]: "
	else
		verbose_str="* "
	fi
	echo -e -n "\e[32m $verbose_str$1 \e[39m"
}


function echo_header () {
	echo ""
	echo "__________________________________________________"
	echo "             ${1^^}"
	echo "__________________________________________________"
}